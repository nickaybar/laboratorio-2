﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_4
{
   public class Empleado : Persona
    {
        public int Sueldo { get; set; }

        public void ObtenerSueldo()
        {
            Console.WriteLine($"El sueldo es: {Sueldo}");
        }
    }
}
