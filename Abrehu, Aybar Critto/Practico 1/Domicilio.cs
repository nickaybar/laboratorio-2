﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practico_1
{
    public class Domicilio
    {
        public string Direccion { get; set; }
        public string Localidad { get; set; }
        public string Provincia { get; set; }


        public Domicilio()
        {

        }

        public Domicilio(string direccion, string localidad, string provincia)
        {
            Direccion = direccion;
            Localidad = localidad;
            Provincia = provincia;
        }

        public string imprimirDomicilio()
        {
            string mensaje;
           return mensaje = $"Direccion: {Direccion}.\nLocalidad: {Localidad}.\nProvincia: {Provincia}";
        }
    }
}
