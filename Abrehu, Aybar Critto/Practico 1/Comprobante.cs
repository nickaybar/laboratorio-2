﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practico_1
{
   public class Comprobante
    {

        public int Cantidad { get; set; }

        public string Detalle { get; set; }

        public decimal PrecioUnitario { get; set; }

        public decimal Importe { get; set; }

        public decimal Total { get; set; }

        public DateTime Fecha { get; set; }
    }
}
