﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLID
{
    public class Estado
    {
        private readonly Dictionary<Type, string> diccionario;

        public Estado()
        {
            diccionario = new Dictionary<Type, string>();
            Inicializador(ref diccionario);
        }

        private void Inicializador(ref Dictionary<Type, string> diccionario)
        {
            diccionario.Add(typeof(Estado), "SOLID.Estado");
            diccionario.Add(typeof(Activo), "SOLID.Activo");
            diccionario.Add(typeof(Anulado), "SOLID.Anulado");
            diccionario.Add(typeof(Cancelado), "SOLID.Cancelado");

        }

        public void AgregarOpcionDiccionario(Type type, string camino)
        {
            diccionario.Add(type, camino);
        }

        public virtual void Cambiar()
        {

        }

    }
}
