﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLID
{
    class Program
    {
        static void Main(string[] args)
        {
            var estado = new Estado();

            estado.AgregarOpcionDiccionario(typeof(Pendiente), "SOLID.Pendiente");

            estado.Cambiar();

            Console.WriteLine(estado.GetType()) ;

            Console.ReadKey();
        }
    }
}
