﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLID
{
    class Factura
    {
        public DateTime Fecha { get; set; }
        public decimal Subtotal { get; set; }
        public decimal Total => CalcularTotal();

        private decimal CalcularTotal()
        {
            decimal _resultado = 0m;
            decimal _iva = 0m;
            decimal _retencion = 0m;

            //calculo iva
            _iva = Iva.CalcularTotal(Subtotal, 21);

            //calculo retenciones
            _retencion = Retencion.Calcular(Subtotal, 3);

            //calculo total
            _resultado = (Subtotal + _iva) - _retencion;
            return _resultado;
        }
    }
}
