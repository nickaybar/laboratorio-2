﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLID
{
    public static class Iva
    {
        public static decimal CalcularTotal(decimal monto, decimal alicuota)
        {
            return monto * (alicuota / 100);
        }
    }
}
