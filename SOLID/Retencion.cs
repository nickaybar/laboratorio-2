﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLID
{
    public static class Retencion
    {
        public static decimal Calcular(decimal monto, decimal alicuota)
        {
            return monto * (alicuota / 100);
        }
    }
}
