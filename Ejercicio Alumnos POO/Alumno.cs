﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_Alumnos_POO
{
   public class Alumno : Persona
    {

        public int Edad => Ejercicio_Alumnos_POO.Edad.Calcular(FechaNacimiento);
        
        //ctor crea constructor vacio

        public Alumno()
        {

        }

        public Alumno(string nombre, string apellido, DateTime fechanacimiento) // Constructor alumno
        {
            this.Nombre = nombre;
            this.Apellido = apellido;
            this.FechaNacimiento = fechanacimiento;
        }


        public void ImprimirDatos()
        {
          Console.WriteLine($"El nombre del alumno es {Nombre}, su apellido es {Apellido} y su fecha de nacimiento es {FechaNacimiento.ToShortDateString()} ");
        }

        //public bool esmayordeedad()
        //{
        //    bool _bandera = false;

        //    if (fechanacimiento <= 2000.tostring()
        //}

        public string esMayorDeEdad(int edad)
        {
            return edad >= 18 ? "El alumno es mayor de edad" : "El alumno es menor de edad";
        }
    }
}
