﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_Alumnos_POO
{
    public static class Edad //clase estatica
    {

        public static int Calcular(DateTime FechaNacimiento)
        {
            var fechaActual = DateTime.Now;

            var edad = fechaActual.Year - FechaNacimiento.Year;

            if (fechaActual.Month < FechaNacimiento.Month)
            {
                edad--;
            }

            else
            {
                if(fechaActual.Day < FechaNacimiento.Day)
                {
                    edad--;
                }
            }

            return edad;
        }
    }
}
