﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_1
{
    public class Cajero
    {
        public decimal saldoTotal;
        
        public decimal MostrarDineroCajero()
        {
            Console.WriteLine($"El dinero recaudado en el cajero es de: ${saldoTotal}");

            return saldoTotal;
        }
    }
}
