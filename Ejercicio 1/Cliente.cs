﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_1
{
    public class Cliente : Persona
    {
        public static int Id;
        public int saldo; // pertenece a cuenta
        private int contadorId;
        
        public Cliente()
        {
           Id = ++contadorId;
        }

        public void Depositar(int dinero) // cuenta
        {
            saldo += dinero;
        }

        public void Extraer(int dinero)
        {
            if (saldo > dinero)
            {
                saldo -= dinero;
            }
            else
            {
                Console.WriteLine($"Usted no puede retirar este monto: ${dinero}, debido a que su saldo actual es: ${saldo} ");
            }

        }

        public decimal MostrarSaldoCliente()
        {

            return saldo;
        }

        public int ObtenerId()
        {
            return Id;
        }

    }
}
