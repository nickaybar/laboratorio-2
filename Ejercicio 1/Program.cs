﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_1
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Cliente> ListaDeClientes = new List<Cliente>();
            Cliente c1 = new Cliente();
            Cliente c2 = new Cliente();
            Cliente c3 = new Cliente();
            Cajero cajero = new Cajero();

            c1.Nombre = "Nicolas";
            c1.Apellido = "Aybar";
            c1.Direccion = "Calle 123";
            c1.Dni = "3943123";

            ListaDeClientes.Add(c1);

            c2.Nombre = "Prueba";
            c2.Apellido = "Dos";
            c2.Direccion = "Calle 123";
            c2.Dni = "39142314";

            ListaDeClientes.Add(c2);

            c3.Nombre = "Prueba";
            c3.Apellido = "Tres";
            c3.Direccion = "Calle 123";
            c3.Dni = "39552321";

            ListaDeClientes.Add(c3);

            c1.Depositar(1000);
            c2.Depositar(2000);
            c3.Depositar(1000);
            
            c1.Extraer(500);
            c2.Extraer(1000);

            foreach (var cliente in ListaDeClientes)
            {
                Console.WriteLine($"{cliente.Apellido}, {cliente.Nombre} - Saldo: ${ cliente.MostrarSaldoCliente()}");
            }

            foreach (var cliente in ListaDeClientes)
            {
                cajero.saldoTotal += cliente.MostrarSaldoCliente();
            }
            
            Console.WriteLine();

            cajero.MostrarDineroCajero();
        
            Console.ReadKey();
        }
    }
}
