﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practico_1
{
    //Laboratorio 2. Comision 2
    // Practico 1
    //Aybar Critto Nicolas. Legajo 49001
    //Abrehu Jorge. Legajo 48995
    class Program
    {


        static void Main(string[] args)
        {

            Cliente Juanchi = new Cliente
            {
                Nombre = "Juan",
                Apellido = "Perez",
                Cuit = "20-38485125-8",
                Email = "juan.perez@gmail.com",
                Telefono = 3816168456,
                DomicilioCliente = ("Caracas", "Yerba buena", "Argentina") // ver
            };

            Juanchi.imprimirDatos();
            Console.ReadKey();
        }
    }
}
