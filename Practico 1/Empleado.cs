﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practico_1
{
   public class Empleado:Persona
    {
        public decimal sueldo { get; set; }
        public int antiguedad { get; set; }
        public Domicilio DomicilioEmpleado;
    }
}
