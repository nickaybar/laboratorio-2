﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practico_1
{
    public class Cliente : Persona
    {
        public Domicilio DomicilioCliente;

        public Cliente()
        {
        }

        Cliente(string nombre, string apellido, string cuit, string email, int telefono, Domicilio domicilio)
        {
            Nombre = nombre;
            Apellido = apellido;
            Cuit = cuit;
            Email = email;
            Telefono = telefono;
            DomicilioCliente = domicilio;
        }

        public void imprimirDatos()
        {
            string mensaje;
            mensaje = $"Cliente: {Nombre} {Apellido}.\nCuit: {Cuit}.\nEmail: {Email}\nTelefono: {Telefono}\nDomicilio: {DomicilioCliente.imprimirDomicilio()}";
            Console.WriteLine(mensaje);
        }
        
    }
}
