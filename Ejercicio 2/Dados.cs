﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_2
{
   public class Dados
    {
        public int cara { get; private set; }
        static Random tirarDados = new Random(); // solved

        public int GirarLosDados()
        {
            
            cara = tirarDados.Next(1, 3);

            return cara;
        }

        public void MostrarCara()
        {
            Console.WriteLine($"La cara del dado es: {cara}");
        }

    }
}
