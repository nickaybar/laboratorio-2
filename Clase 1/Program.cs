﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Clase_1;

namespace Clase_POO
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Empleado> ListaDeEmpleados = new List<Empleado>();

            //CAJERO
            Empleado cajero = new Cajero
            {
                Apellido = "Rodriguez",
                Nombre = "Nicolas"
            };
            ListaDeEmpleados.Add(cajero);


            //JEFE
           
            Empleado jefe = new Jefe
            {
                Apellido = "Cuello",
                Nombre = "Juana",
              
            };
            ListaDeEmpleados.Add(jefe);


            //GERENTE
    
            Empleado gerente = new Gerente
            {
                Apellido = "Daud",
                Nombre = "Cristian"
            };
            ListaDeEmpleados.Add(gerente);

            //EJECUCION

            foreach (var empleado in ListaDeEmpleados)
            {
                Console.WriteLine($"{empleado.Apellido}, {empleado.Nombre} -Sueldo: { empleado.MostrarSueldo()}");
            }
            Console.ReadKey();
        }
    }
}
