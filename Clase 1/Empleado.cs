﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clase_1
{
    class Empleado : Persona
    {
        private decimal SueldoBase;
        public string Legajo { get; set; }
        public DateTime FechaIngreso { get; set; }

        public Empleado()
        {
            SueldoBase = 10000m;
        }

        public virtual decimal MostrarSueldo()
        {
            return SueldoBase;
        }
    }
}
