﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clase_1
{
    class Cliente : Persona
    {
        public decimal MaximoCompra { get; set; }
        public string Dni { get; set; }
    }
}
