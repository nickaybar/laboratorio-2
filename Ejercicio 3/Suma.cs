﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio3
{
    public class Suma : Operaciones
    {

        public int OperarSuma(int Valor1, int Valor2)
        {
            resultado = Valor1 + Valor2;

            return resultado;
        }

        public void ObtenerSuma()
        {
            Console.WriteLine($"El resultado de la suma entre Valor1 y Valor2 es igual a: {resultado}");
        }

    }
}
